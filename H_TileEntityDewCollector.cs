﻿namespace Dewta16SlotPatch
{ 
    using System;
    using HarmonyLib;

    public class H_TileEntityDewCollector
    {
        [HarmonyPatch(typeof(TileEntityDewCollector), MethodType.Constructor, new System.Type[] { typeof(Chunk) }), HarmonyPatch("TileEntityDewCollector")]
        public class H_TileEntityDewCollector_Constructor
        {
            private static void Postfix(ref Vector2i ___containerSize)
            {
                ___containerSize = new Vector2i(4, 4);
            }
        }
    }
}
