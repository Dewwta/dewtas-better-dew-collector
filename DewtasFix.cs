﻿using System;
using HarmonyLib;
using System.Reflection;
using UnityEngine;

namespace Dewta16SlotPatch
{
    public class DewtasFix : IModApi
    {
        public void InitMod(Mod _modInstance)
        {
            new Harmony(base.GetType().ToString()).PatchAll(Assembly.GetExecutingAssembly());
            
        }

    }
  
}
